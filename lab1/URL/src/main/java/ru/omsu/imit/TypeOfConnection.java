package ru.omsu.imit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public abstract class TypeOfConnection {
    private String url;
    private String fileName;

    public abstract void connect();

}
