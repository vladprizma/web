package ru.omsu.imit;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class URLConnectionType extends TypeOfConnection {

    public URLConnectionType(String url, String filePath){
        super(url, filePath);
    }

    @Override
    public void connect() {
        try{
            URL url = new URL(this.getUrl());
            URLConnection urlCon = url.openConnection();

            // Header output
            Map<String, List<String>> map = urlCon.getHeaderFields();
            for (String key : map.keySet()) {
                System.out.println(key + ":");

                List<String> values = map.get(key);

                for (String aValue : values) {
                    System.out.println("\t" + aValue);
                }
            }

            // Content output
            InputStream inputStream = urlCon.getInputStream();
            BufferedInputStream reader = new BufferedInputStream(inputStream);

            BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(this.getFileName()));
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, bytesRead);
            }

            writer.close();
            reader.close();

        } catch (MalformedURLException e) {
            System.out.println("The specified URL is malformed: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("An I/O error occurs: " + e.getMessage());
        }
    }
}
