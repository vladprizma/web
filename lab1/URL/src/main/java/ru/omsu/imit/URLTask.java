package ru.omsu.imit;

import java.util.Scanner;

public class URLTask {

    public static void main(String[] args) {
        int amountOfConnections = 2;
        try (Scanner s = new Scanner(System.in)) {
            System.out.print("Enter URL:");
            String url = s.next();
            System.out.print("Enter file name:");
            String fileName = s.next();
            System.out.println("Possible connections:");
            System.out.println("1.Socket");
            System.out.println("2.URLConnection");
            System.out.print("Enter number of connection:");
            int typeOfConnection = s.nextInt();

            if (typeOfConnection > 2 || typeOfConnection < 1) {
                System.out.print("There are only " + amountOfConnections + " types of connection");
                return;
            }

            TypeOfConnection type;

            switch (typeOfConnection) {
                case (1):
                    type = new SocketType(url, fileName);
                    break;
                case (2):
                    type = new URLConnectionType(url, fileName);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + typeOfConnection);
            }

            type.connect();

            System.out.println("Web page content saved in project root directory (URL) in file " + fileName);
        }
    }

}
