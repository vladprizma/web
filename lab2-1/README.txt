Файлы для запуска веб сервера находятся в папке ./checkers/out

Параметры <port> и <path> задаются файлом конфигурации: application.properties

Сервер запускается командой: java -jar checkers.jar

Страница с игрой доступна по адресу: http://localhost:<port>/<path>/