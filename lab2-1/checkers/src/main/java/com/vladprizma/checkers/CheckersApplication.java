package com.vladprizma.checkers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// https://javarush.ru/groups/posts/1352-kak-sozdatjh-ispolnjaemihy-jar-v-intellij-idea--how-to-create-jar-in-idea

// https://javarevisited.blogspot.com/2010/10/why-string-is-immutable-or-final-in-java.html
// https://javarevisited.blogspot.com/2011/02/how-hashmap-works-in-java.html
// https://javarevisited.blogspot.com/2015/10/133-java-interview-questions-answers-from-last-5-years.html

// решением ClassNotFoundException у меня было с помощью удаления пробела в пути к классу

@SpringBootApplication
public class CheckersApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckersApplication.class, args);
	}

}
